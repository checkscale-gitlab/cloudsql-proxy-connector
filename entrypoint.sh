#!/bin/sh
set -e

# if credentials are passed as environment variable this code will save it to disk
# otherwise, it will use file path passed as environment variable

credential_file=""

if [ -n "$GCLOUD_CREDENTIALS_FILE" ]; then
  credential_file="-credential_file=$GCLOUD_CREDENTIALS_FILE"
elif [ -n "$GCLOUD_CREDENTIALS" ]; then
  echo $GCLOUD_CREDENTIALS > /tmp/credentials.json
  credential_file="-credential_file=/tmp/credentials.json"
fi

/cloud_sql_proxy -dir=/cloudsql -instances=$CLOUDSQL_INSTANCE "${credential_file}" & exec "$@"
