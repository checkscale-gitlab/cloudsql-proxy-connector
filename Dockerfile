FROM gcr.io/cloudsql-docker/gce-proxy as cloudsql-proxy

FROM node:alpine

# ca-certificates is needed to securely connect to googleapis.com
RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*
# create directory for cloudsql-proxy unix socket
RUN mkdir /cloudsql && chmod 777 /cloudsql

# copy cloudsql-proxy binary from latest official docker image
COPY --from=cloudsql-proxy /cloud_sql_proxy /cloud_sql_proxy
COPY entrypoint.sh /usr/local/bin/

ENTRYPOINT ["entrypoint.sh"]

