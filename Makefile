.PHONY: build
build: Dockerfile
	docker build --pull -t registry.gitlab.com/gazanchyand/cloudsql-proxy-connector .
	docker push registry.gitlab.com/gazanchyand/cloudsql-proxy-connector
